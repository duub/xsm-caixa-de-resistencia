# XSM Caixa de Resistència

Aquest codi serveix per a crear un bot de telegram i utilitzar-lo per a gestionar les compres a botigues inscrites a una caixa de resistència.

Utilitza aquesta API: https://github.com/davtur19/TuriBot

Per fer-lo funcionar cal crear una base de dades amb les taules que hi ha en el fitxer .sql i només caldrà omplir la taula amb els usuaris que participin de la caixa de resistència.
Després també caldrà seguir el procediment del botfather per crear un nou bot i penjar els fitxers al servidor.