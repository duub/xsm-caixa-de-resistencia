<?php

require_once __DIR__ . "/vendor/autoload.php";

use TuriBot\Client;

error_reporting(E_ERROR | E_WARNING | E_PARSE);

if (!isset($_GET["api"])) {
  exit();
}

$client = new Client($_GET["api"], false);
$update = $client->getUpdate();
if (!isset($update)) {
  exit('json error');
}

//Connectar base de dades
$servername = "mysql-server-url";
$username = "database-username";
$password = "database-password";
$dbname = "database-name";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if (!$conn->set_charset("utf8")) {
  printf("Error loading character set utf8: %s\n", $mysqli->error);
  exit();
} else {
  printf("Current character set: %s\n", $conn->character_set_name());
}

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

//variables globals
$menu["inline_keyboard"] = [
  [
    [
      "text"          => "Cobrar",
      "callback_data" => "cobrar",
    ],
  ],
  [
    [
      "text"          => "Saldo",
      "callback_data" => "saldo",
    ],
  ],
  [
    [
      "text"          => "Cancel·lar acció",
      "callback_data" => "cansalada",
    ],
  ],
];

if (isset($update->message) or isset($update->edited_message)) {
  $chat_id = $client->easy->chat_id;
  $message_id = $client->easy->message_id;
  $text = $client->easy->text;

  $text = $conn->real_escape_string($text);

  //$client->sendMessage($chat_id, "username:". $userID);
  //$chat = $client->getChat($chat_id);
  //$client->sendMessage($chat_id, "vardump: " . print_r($chat,TRUE));
  //$client->sendMessage($chat_id, "usuari: " . $chat->result->username);
  //$client->sendMessage($chat_id, "id: " . $chat->result->id);

  // afegir al registre el missatge rebut
  $sql = "INSERT INTO registre (missatge, chatID) VALUES ('" . $text . "'," . $chat_id . ")";
  if ($conn->query($sql) === FALSE) {
    $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
  }

  if ($text === "/start") {
    $client->sendMessage($chat_id, "Hola! Sóc el bot de la Xarxa de Suport de Vilamajor, des d'aquí gestionarem les compres facilment!");
    $chat = $client->getChat($chat_id);
    $client->sendMessage($chat_id, "usuari: " . $chat->result->username);
    $client->sendMessage($chat_id, "id: " . $chat->result->id);
  }
  //comprovar que l'usuari no estigui ja a la base de dades
  $sql = "SELECT ID,estat FROM botigues WHERE usuariID=" . $client->easy->from_id;
  $botiga = $conn->query($sql)->fetch_assoc();
  if ($botiga) {
    //ha trobat un resultat d'usuariID
    if($botiga["estat"] == 0){
      //Guardar nom botiga
      $sql = "UPDATE botigues SET nomBotiga='" . $text . "', estat=1 WHERE ID=" . $botiga["ID"];
      if ($conn->query($sql) === FALSE) {
        $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
      }else{
        $client->sendMessage($chat_id, "Hola, ".$text);
        $client->sendMessage($chat_id, "Ara introdueix l'adreça");
      }

    }elseif ($botiga["estat"] == 1) {
      //Guardar adreça botiga
      $sql = "UPDATE botigues SET adresa='" . $text . "', estat=2 WHERE ID=" . $botiga["ID"];
      $client->sendMessage($chat_id, $sql);
      if ($conn->query($sql) === FALSE) {
        $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
      }
      $a = $client->sendMessage($chat_id, "Què vols fer?", null, null, null, null, $menu);
    }elseif ($botiga["estat"] == 2) {
      //recuperar accions
      //si acció és buida mostrar menú
      //si acció és cobrar i estat=2 demanar quantitat a Cobrar
      //si acció és saldo i estat=2 mostrar Saldo
      $sql = "SELECT accio,estat,DNI FROM accions WHERE usuariID=" . $client->easy->from_id;
      $accio = $conn->query($sql)->fetch_assoc();
      if ($accio == FALSE) {
        $a = $client->sendMessage($chat_id, "Què vols fer?", null, null, null, null, $menu);
      }elseif($accio["accio"]=="cobrar"){
        if($accio["estat"]==2){
          //tinc el dni, miro el saldo, demano import
          $sql = "SELECT Saldo FROM usuaris WHERE DNI='" . $text."'";
          $usuari = $conn->query($sql)->fetch_assoc();
          if ($usuari) {
            $client->sendMessage($chat_id, "té ".$usuari["Saldo"]. "€ de saldo.");
            $client->sendMessage($chat_id, "Import a cobrar?");
            $sql = "UPDATE accions SET estat=1, DNI='".$text."' WHERE usuariID=" . $chat_id;
            if ($conn->query($sql) === FALSE) {
              $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
            }
          }else{
            $client->sendMessage($chat_id, "DNI ".$text." no trobat, format correcte: 12345678A.");
            $client->sendMessage($chat_id, "Torna a introduir un DNI:");
          }
        }elseif ($accio["estat"]==1) {
          $import = floatval(str_replace(',', '.', $text));
          // rebem import acobrar, recuperar DNI de acció,
          // comprovar que hi ha saldo, registrar cobrament o no, esborrar accio
          $sql = "SELECT Saldo FROM usuaris WHERE DNI='" . $accio["DNI"] ."'";
          $usuari = $conn->query($sql)->fetch_assoc();
          if ($usuari) {
            if ($import <= $usuari["Saldo"]){
              //registrem el cobrament
              $sql = "INSERT INTO cobraments (botigaID, DNI, import) VALUES (". $chat_id .",'". $accio["DNI"] ."'," . $import.")";
              if ($conn->query($sql) === FALSE) {
                $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
              }
              //restem saldo
              $saldo = $usuari["Saldo"] - $import;
              $client->sendMessage($chat_id, "Saldo restant:". $saldo);
              $sql = "UPDATE usuaris SET Saldo=" . $saldo . " WHERE DNI='" . $accio["DNI"]."'";
              if ($conn->query($sql) === FALSE) {
                $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
              }
              //eliminar acció
              $sql = "DELETE FROM accions WHERE usuariID=".$chat_id;
              if ($conn->query($sql) === FALSE) {
                $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
              }
              $a = $client->sendMessage($chat_id, "Què vols fer?", null, null, null, null, $menu);
            } else {
              $client->sendMessage($chat_id, "No hi ha prou saldo");
              $sql = "DELETE FROM accions WHERE usuariID=".$chat_id;
              if ($conn->query($sql) === FALSE) {
                $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
              }
              $a = $client->sendMessage($chat_id, "Què vols fer?", null, null, null, null, $menu);
            }
          }
        }
      }elseif($accio["accio"]=="saldo"){
        $DNI = $text;
        $sql = "SELECT Saldo FROM usuaris WHERE DNI='" . $DNI ."'";
        $usuari = $conn->query($sql)->fetch_assoc();
        if ($usuari) {
          $client->sendMessage($chat_id, "Saldo: " . $usuari["Saldo"]."€");
        }else {
          $client->sendMessage($chat_id, "DNI: " . $usuari["Saldo"] . " no registrat.");
        }
      }
    } else {
      //  Primera vegada que l'usuariID contacta amb el BOT
      $client->sendMessage($chat_id, "No hi ha la botiga registrada, introdueix el nom de la botiga");
      $sql = "INSERT INTO botigues (usuariID,estat) VALUES (" . $client->easy->from_id . ",0)";
      if ($conn->query($sql) === TRUE) {
      } else {
        $client->sendMessage($chat_id, "Error: " . $sql . "\n" . $conn->error);
      }
    }

    if ($text === "/user") {
      $client->sendMessage($chat_id, 'Usuari:' . $client->easy->from_id);
    }
  }
}
if (isset($update->callback_query)) {

  $id = $update->callback_query->id;
  $message_chat_id = $update->callback_query->message->chat->id;
  $message_message_id = $update->callback_query->message->message_id;

  if ($update->callback_query->data === "cobrar") {
    $client->sendMessage($message_chat_id, "Introdueix el DNI:");
    $sql = "INSERT INTO accions (usuariID,accio,estat) VALUES (".$message_chat_id.",'cobrar',2) ON DUPLICATE KEY UPDATE accio='cobrar', estat=2";
    if ($conn->query($sql) === FALSE) {
      $client->sendMessage($message_chat_id, "Error: " . $sql . "\n" . $conn->error);
    }
  } elseif ($update->callback_query->data === "saldo") {
    $client->sendMessage($message_chat_id, "Introdueix el DNI:");
    $sql = "INSERT INTO accions (usuariID,accio,estat) VALUES (".$message_chat_id.",'saldo',2) ON DUPLICATE KEY UPDATE accio='saldo', estat=2";
    if ($conn->query($sql) === FALSE) {
      $client->sendMessage($message_chat_id, "Error: " . $sql . "\n" . $conn->error);
    }
  } elseif ($update->callback_query->data === "cansalada") {
    //Recuperar DNI i esborrar accions desades a la base de dades
    $sql = "DELETE FROM accions WHERE usuariID=".$message_chat_id;
    if ($conn->query($sql) === FALSE) {
      $client->sendMessage($message_chat_id, "Error: " . $sql . "\n" . $conn->error);
    }
    $a = $client->sendMessage($message_chat_id, "Què vols fer?", null, null, null, null, $menu);
  }
}
//tancar connexió base de dades
$conn->close();
?>
