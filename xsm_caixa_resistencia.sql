-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 18-04-2020 a les 15:31:53
-- Versió del servidor: 5.7.23-23-log
-- Versió de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `xsm_caixa_resistencia`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `accions`
--

CREATE TABLE `accions` (
  `usuariID` int(11) NOT NULL,
  `accio` text CHARACTER SET utf8 NOT NULL,
  `estat` int(11) NOT NULL,
  `DNI` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registre de les accions dels usuaris';


-- --------------------------------------------------------

--
-- Estructura de la taula `botigues`
--

CREATE TABLE `botigues` (
  `ID` int(11) NOT NULL,
  `usuariID` int(11) NOT NULL,
  `nomBotiga` text,
  `adresa` text,
  `estat` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='botigues que fan ús del bot';


-- --------------------------------------------------------

--
-- Estructura de la taula `cobraments`
--

CREATE TABLE `cobraments` (
  `ID` int(11) NOT NULL,
  `botigaID` int(11) NOT NULL,
  `DNI` text NOT NULL,
  `import` float NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estructura de la taula `registre`
--

CREATE TABLE `registre` (
  `ID` int(11) NOT NULL,
  `missatge` text NOT NULL,
  `chatID` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='registre missatges';


-- --------------------------------------------------------

--
-- Estructura de la taula `usuaris`
--

CREATE TABLE `usuaris` (
  `DNI` text NOT NULL,
  `Nom` text NOT NULL,
  `Saldo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Índexs per a la taula `accions`
--
ALTER TABLE `accions`
  ADD UNIQUE KEY `usuariID` (`usuariID`);

--
-- Índexs per a la taula `botigues`
--
ALTER TABLE `botigues`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `usuariID` (`usuariID`);

--
-- Índexs per a la taula `cobraments`
--
ALTER TABLE `cobraments`
  ADD PRIMARY KEY (`ID`);

--
-- Índexs per a la taula `registre`
--
ALTER TABLE `registre`
  ADD PRIMARY KEY (`ID`);

--
-- Índexs per a la taula `usuaris`
--
ALTER TABLE `usuaris`
  ADD UNIQUE KEY `DNI` (`DNI`(9));


--
-- AUTO_INCREMENT per la taula `botigues`
--
ALTER TABLE `botigues`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la taula `cobraments`
--
ALTER TABLE `cobraments`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la taula `registre`
--
ALTER TABLE `registre`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
